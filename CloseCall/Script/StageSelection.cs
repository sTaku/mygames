﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StageSelection : MonoBehaviour
{
    [SerializeField]
    public GameObject e_startMenu;
    public GameObject e_stageSelect;
    public RectTransform Select;

    // Use this for initialization
    void Start ()
    {
        e_stageSelect.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
	}
    
    public void ButtonSelected()
    {
        PlaySE(0);
    }

    public void StageSelect()
    {
        e_startMenu.SetActive(false);
        e_stageSelect.SetActive(true);
        PlaySE(0);
    }
    public void ChangeStageSelect(int positionX)
    {
        LeanTween.move(Select, new Vector2(positionX, 0), 0.2f);
        PlaySE(0);
    }

    public void Back()
    {
        e_startMenu.SetActive(true);
        e_stageSelect.SetActive(false);
        PlaySE(0);
    }

    private void PlaySE(int number)
    {
        SoundManger.Instance.PlaySE(number);
    }

    public void GoToStage()
    {
        PlaySE(0);
        SoundManger.Instance.StopBGM();
    }
}
