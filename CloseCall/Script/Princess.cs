﻿using UnityEngine;
using System.Collections;

public class Princess : MonoBehaviour
{
    [SerializeField]
    private int hp2;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Electricity")
        {
            hp2 -= 2;
        }
        if (other.tag == "Enemy")
        {
            hp2 -= 1;
        }
    }

    void Awake()
    {
        hp2 = 20;
        DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hp2 <= 0)
        {
            Destroy(gameObject);
            SoundManger.Instance.PlaySE(3);
        }

        float colorSet = hp2 / 40.0f + 0.4f;
        GetComponent<SpriteRenderer>().color = new Color(colorSet, colorSet, colorSet, 1.0f);

        float animeSpeed = hp2 / 40.0f +0.5f;
        GetComponent<Animator>().speed = animeSpeed;
    }

    public float GetHpValue()
    {
        //return hp / hpMaxValue;
        return 0;
    }

    public int Hp2()
    {
        return hp2;
    }
}
