﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class ResultSetter : MonoBehaviour
{
    private GameObject resultGetter;
    public GameObject s_UIs_GameClear;
    public GameObject s_UIs_GameOver;
    private GameObject princess;
    private GameObject sceneChange;
    private GameObject blackOut;
    private NoiseAndScratches noise;
    private GameObject button_NextStage;
    private float m_a = 1;
    public bool setResult = false;
    //string nowScene;
    static string playedScene;

    // Use this for initialization
    void Start()
    {
        noise = Camera.main.GetComponent<NoiseAndScratches>();
        noise.enabled = false;
        blackOut = GameObject.Find("FadeImage");
        resultGetter = GameObject.Find("ResultGetter");
        //s_UIs_GameClear = GameObject.Find("UIs_GameClear");
        //s_UIs_GameOver = GameObject.Find("UIs_GameOver");
        princess = GameObject.Find("Princess");
        sceneChange = GameObject.Find("SceneManager");
        button_NextStage = GameObject.Find("Crear_NextStage");
        playedScene = resultGetter.GetComponent<ResultGetter>().PlayedScene();
        if (princess != null)
        {
            setResult = true;
            Destroy(princess);
        }
        //else
        //{
        //    Destroy(princess);
        //}

        if (setResult)
        {
            Setting_GameClear();
        }
        else
        {
            Setting_GameOver();
        }

        if (playedScene == "Stage_15")
        {
            button_NextStage.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (setResult == false)
        {
            noise.grainIntensityMax -= 1.5f * Time.deltaTime;
            blackOut.GetComponent<Image>().color = new Color(0, 0, 0, m_a);
            m_a -=0.1f * Time.deltaTime;

        }
        if(noise.grainIntensityMax == 0)
        {
            noise.enabled = false;
        }
    }

    void Setting_GameClear()
    {
        s_UIs_GameClear.SetActive(true);
        s_UIs_GameOver.SetActive(false);
    }

    void Setting_GameOver()
    {
        StartCoroutine("NoiseEffect");
        s_UIs_GameClear.SetActive(false);
        s_UIs_GameOver.SetActive(true);
    }

    public void ReTry()
    {
        if (resultGetter != null)
        {
            sceneChange.GetComponent<SceneChangeScript>().FadeOut(playedScene);
            Destroy(resultGetter);
        }
        if (princess != null)
        {
            Destroy(princess);
        }
        PlaySE(0);
        StopBGM();
    }

    public void BackToTitle()
    {
        sceneChange.GetComponent<SceneChangeScript>().FadeOut("Title");
        Destroy(resultGetter);
        if (princess != null)
        {
            Destroy(princess);
        }
        PlaySE(1);
        StopBGM();
    }

    public void GoToNextStage()
    {
        if (resultGetter != null)
        {
            #region Old Script
            /*
            char splitPoint = 'e';
            playedScene = resultGetter.GetComponent<ResultGetter>().PlayedScene();
            string[] separatedStageName = playedScene.Split(splitPoint);
            //Debug.Log(separatedStageName[1]);
            int stageNumber = int.Parse(separatedStageName[1]);
            stageNumber++;
            string nextStage = "Stage" + stageNumber;
            //Debug.Log(nextStage);
            sceneChange.GetComponent<SceneChangeScript>().FadeOut(nextStage);
            */
            #endregion
    
            char splitPoint = '_';
            string[] separatedStageName = playedScene.Split(splitPoint);
            int stageNumber = int.Parse(separatedStageName[1]);
            stageNumber++;
            string nextStage = separatedStageName[0] + splitPoint + stageNumber;
            sceneChange.GetComponent<SceneChangeScript>().FadeOut(nextStage);
        }
        if (princess != null)
        {
            Destroy(princess);
        }
        PlaySE(0);
        StopBGM();
    }

    private void PlaySE(int number)
    {
        SoundManger.Instance.PlaySE(number);
    }

    private void StopBGM()
    {
        SoundManger.Instance.StopBGM();
    }

    IEnumerator NoiseEffect()
    {
        Debug.Log("a");
        noise.enabled = true;
        yield return new WaitForSeconds(0.2f);
        noise.enabled = false;
        yield return new WaitForSeconds(0.2f);
        noise.enabled = true;
    }
}
