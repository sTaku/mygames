﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class HpGauge : MonoBehaviour
{
    [SerializeField]
    private float hp;//実際の値
    private float outputValue;//出力されている値
    private int waitCount = 30;//赤ゲージが減り始めるまでの時間
    private int fadeOutCount = 120;//フェードアウトまでの時間
    private GameObject princess;
    private GameObject sceneChange;
    private GameObject gauge_Green;
    private GameObject gauge_Red;
    public GameObject gauge_Frame;

    private NoiseAndScratches noise;
    private float r;
    private float g;
    private float b;

    // Use this for initialization
    void Start()
    {
        noise = Camera.main.GetComponent<NoiseAndScratches>();
        sceneChange = GameObject.Find("SceneManager");
        princess = GameObject.Find("Princess");
        gauge_Green = GameObject.Find("HpGauge_Bar");
        gauge_Red = GameObject.Find("HpGauge_Bar2");
        //gauge_Frame = GameObject.Find("HpGauge_Frame");
        outputValue = princess.GetComponent<Princess>().GetHpValue();
        noise.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        waitCount--;
        gauge_Green.GetComponent<Image>().fillAmount = hp;
        gauge_Red.GetComponent<Image>().fillAmount = outputValue;

        //print(outputValue);
        if (outputValue > hp && waitCount <= 0)
        {
            outputValue -= 0.005f;
        }
        if (outputValue < hp)
        {
            outputValue += 0.005f;
        }
        if (Mathf.Abs(hp - outputValue) < 0.0005f)
        {
            outputValue = hp;
            SetCountDefault();
        }
        if (princess != null)
        {
            //HPの割合をPrincessから取得
            //Princessのhp自体はPrincess.csにある
            hp = princess.GetComponent<Princess>().GetHpValue();
            //print(hp);
            if (hp <= 0.3f)
            {
                gauge_Frame.GetComponent<Animator>().enabled = true;
            }
            r = hp / 2 + 0.5f;
            g = hp / 2 + 0.5f;
            b = hp / 2 + 0.5f;
            princess.GetComponent<SpriteRenderer>().color = new Color(r, g, b, 1.0f);
        }

        if (princess == null)
        {
            hp = 0;
            //Debug.Log(hp);
            //Debug.Log(outputValue);
            if (outputValue < 0.01f)
            {
                outputValue = 0;
            }
            //ゲージの表示が0になってから60フレーム後にフェードアウト開始
            if (outputValue <= 0)
            {
                noise.enabled = true;
                fadeOutCount--;
            }
            if (fadeOutCount <= 0)
            {
                sceneChange.GetComponent<SceneChangeScript>().FadeOut("Result");
            }
        }
    }

    void SetCountDefault()
    {
        waitCount = 15;
    }

}
