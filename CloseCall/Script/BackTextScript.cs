﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackTextScript : MonoBehaviour
{
    private float alpha;
    private float alphaChange;
    //private float alphaChangeSpeed_0;
    public float alphaChangeSpeed_1;
    public float alphaChangeSpeed_2;

	// Use this for initialization
	void Start ()
    {
        alphaChange = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        alpha = (Mathf.Sin(alphaChange * Mathf.PI / 360f) * 9 / 20) + 0.55f;
        if (alpha > 0.99f)
        {
            alphaChange += alphaChangeSpeed_2;
        }
        if (alpha <= 0.99f)
        {
            alphaChange += alphaChangeSpeed_1;
        }
        //alphaChange += alphaChangeSpeed_0;
        GetComponent<Text>().color = new Color(0.0f, 0.8f, 0.1f, alpha);
    }
}
