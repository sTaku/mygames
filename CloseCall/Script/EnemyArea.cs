﻿using UnityEngine;
using System.Collections;

public class EnemyArea : MonoBehaviour {

    public GameObject EnemyAppereanceobj;
    public GameObject Enemy;
    public GameObject obj;
    public GameObject m_GameMane;
    public Transform EnemytoPoint,EnemytoPoint2;
    public float EnemymoveTime, EnemymoveTime2;


    // Use this for initialization
    void Start () {
        m_GameMane = GameObject.Find("GameMane");
    }
	
	// Update is called once per frame
	void Update () {
	}
    public void EnemyGeneration()
    {
        Instantiate(EnemyAppereanceobj, this.transform.position, Quaternion.Euler(0, 0, 0));
        StartCoroutine("EnemyAppearance");
    }
    IEnumerator EnemyAppearance()
    {
        yield return new WaitForSeconds(0.9f);
        obj = (GameObject)Instantiate(Enemy, this.transform.position, Quaternion.Euler(0, 0, 0));
        obj.transform.parent = transform;
    }
}
