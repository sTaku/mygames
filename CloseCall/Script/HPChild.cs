﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPChild : MonoBehaviour
{
    private GameObject princess;
    public int hpCounter;
    private float a;
    private int chargeTime;
    private int hp;
    public GameObject _BurstParticle;

	// Use this for initialization
	void Start ()
    {
        princess = GameObject.Find("Princess");
        a = 0.0f;
        chargeTime = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        chargeTime++;
        if (chargeTime == hpCounter * 3 + 150)
        {
            a = 1.0f;
        }

        GetComponent<Image>().color = new Color(1, 1, 1, a);

        hp = princess.GetComponent<Princess>().Hp2();
        if (princess == null)
        {
            hp = 0;
        }

        if (hp < hpCounter)
        {
            Instantiate(_BurstParticle, transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(gameObject);
        }
    }
}
