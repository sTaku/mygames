﻿using UnityEngine;
using System.Collections;

public class EnemyRote : MonoBehaviour
{

    [SerializeField]
    Transform Topoint1;
    public float MoveTime1;
    [SerializeField]
    Transform Topoint2;
    public float MoveTime2;
    [SerializeField]
    Transform Topoint3;
    public float MoveTime3;
    public Vector2 toMovePoint;
    public float toMoveTime;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }
    public Vector2 MovePoint()
    {
        return toMovePoint;
    }

    public float MoveTime()
    {
        return toMoveTime;
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            float random = Random.Range(0, 3);
            if (random == 0)
            {
                toMovePoint = Topoint1.position;
                toMoveTime = MoveTime1;
                
            }

            if(random == 1)
            {
                toMovePoint = Topoint2.position;
                toMoveTime = MoveTime2;
            }

            if(random == 2)
            {
                toMovePoint = Topoint3.position;
                toMoveTime = MoveTime3;
            }
        }
    }
}
