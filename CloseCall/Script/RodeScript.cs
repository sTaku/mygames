﻿using UnityEngine;
using System.Collections;

public class RodeScript : MonoBehaviour
{
    [SerializeField]
    Vector2 position;
    [SerializeField]
    float waitTime;
    //色指定       赤　　緑　　青
    private float red, green, blue;
    private float colordown;
    private bool downflag;
    // Use this for initialization
    void Start ()
    {
        red = 0.9f;
        green = 1.0f;
        blue = 1.0f;
        colordown = 0.01f;
        downflag = true;
        GetComponent<SpriteRenderer>().color = new Color(red, green, blue, 1.0f);
        LeanTween.move(gameObject, position, 0.7f)
            .setEase(LeanTweenType.easeOutCubic)
            .setDelay(waitTime + 1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (GamePlayManagerScript.GamePlayManager.waitFrag == false) return;
        if (blue >= 1.0f) downflag = true;
        else if (blue <= 0.5f) downflag = false;
        if (downflag == true)
        {
            red         -= colordown;
            green       -= colordown;
            blue        -= colordown;
        }
        else
        {
            red         += colordown;
            green       += colordown;
            blue        += colordown;
        }

        GetComponent<SpriteRenderer>().color = new Color(red, green, blue, 1.0f);
    }
}
