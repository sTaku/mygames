﻿using UnityEngine;
using System.Collections;

public class ElectricityRote : MonoBehaviour
{

    public float SwitchRotaition, SwitchRotaitionR;

    [SerializeField]
    float number;
    public bool Switch = false;
    public GameObject[] lingObject;

    [SerializeField]
    Transform Topoint1;
    public float MoveTime1;
    [SerializeField]
    Transform Topoint2;
    public float MoveTime2;
    //電気の移動先の座標
    public Vector2 toMovePoint;
    public float toMoveTime;


    [SerializeField]
    float enemyRotateA;
    [SerializeField]
    float enemyRotateB;

    // Use this for initialization
    void Start()
    {
        if(Topoint1 != null) toMovePoint = Topoint1.position;
        toMoveTime = MoveTime1;
    }

    // Update is called once per frame
    void Update()
    {
        /*Vector2 tapPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D collider = Physics2D.OverlapPoint(tapPoint);
        if (collider != null)
        {
            if (Input.GetMouseButtonDown(0) && (Switch == false))
            {
                GameObject obj = collider.transform.gameObject;
                if(obj.GetComponent<ElectricityRote>().link == 0) return;
                if (obj.GetComponent<ElectricityRote>().link== link)
                {
                    this.transform.Rotate(0, 0, SwitchRotaition);
                    Switch = true;
                    child.SendMessage("Switchon", 1);
                }

            }
            else if (Input.GetMouseButtonDown(0) && (Switch == true))
            {
                GameObject obj = collider.transform.gameObject;
                if (obj.GetComponent<ElectricityRote>().link == link)
                {
                    this.transform.Rotate(0, 0, SwitchRotaitionR);
                    Switch = false;
                    child.SendMessage("Switchoff", 1);
                }
            }
        }*/
}
    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Enemy")
        {
            float random = Random.Range(0, 2);
            if (random == 0)
            {
                collision.transform.Rotate(0, 0, enemyRotateA, Space.World);
            }
            if (random == 1)
            {
                collision.transform.Rotate(0, 0, enemyRotateB, Space.World);
            }
        }
    }
    public void OnMouseDown()
    {
        if (number == 0) return;
        Rotate();
        if(lingObject[0] != null)
        {
            for (int i = 0; i < number; i++)
            {
                lingObject[i].GetComponent<ElectricityRote>().Rotate();
            }
        }
    }

    public void Rotate()
    {
        if (Switch == false && Topoint2 != null)
        {
            this.transform.Rotate(0, 0, SwitchRotaition);
            toMovePoint = Topoint2.position;
            toMoveTime = MoveTime2;
            Switch = true;
        }

        else if (Switch == true && Topoint1 != null)
        {
            this.transform.Rotate(0, 0, SwitchRotaitionR);
            toMovePoint = Topoint1.position;
            toMoveTime = MoveTime1;
            Switch = false;
        }
    } 
    public Vector2 MovePoint()
    {
        return toMovePoint;
    }

    public float MoveTime()
    {
        return toMoveTime;
    }
}

