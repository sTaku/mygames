﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
        
    public GameObject[] EnemyArea;
    public float EnemyCount  =0;
    public float EnemyCountmax;
    public float[] EnemyAreaCount;
    private float[] EnemyTotalCount = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private float[] Appearancetime = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    public float[] AppearancetimeMax;

    public int AreaCount;

    // Use this for initialization
    void Start (){
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (GamePlayManagerScript.GamePlayManager.waitFrag == false) return;
        
        for( int i = 0; i < AreaCount; i ++)
        {
            if ((EnemyCount <= EnemyCountmax) && (EnemyTotalCount[i] <= EnemyAreaCount[i]))
            {
                Appearancetime[i] += Time.deltaTime;
                if (Appearancetime[i] > AppearancetimeMax[i])
                {
                    EnemyArea[i].GetComponent<EnemyArea>().EnemyGeneration();
                    EnemyCount++;
                    Appearancetime[i] = 0;
                    EnemyTotalCount[i]++;
                }
            }
        }
    }
}
        