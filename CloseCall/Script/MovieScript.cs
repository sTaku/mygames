﻿using UnityEngine;
using System.Collections;

public class MovieScript : MonoBehaviour
{
    MovieTexture movie;
    // Use this for initialization
    void Start ()
    {
        Renderer r = GetComponent<Renderer>();
        movie = (MovieTexture)r.material.mainTexture;
        movie.Play();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.anyKeyDown ||
            Input.GetMouseButtonDown(0) ||
            Input.GetMouseButtonDown(1) ||
            Input.GetMouseButtonDown(2) ||
            movie.isPlaying != true)
        {
            SceneChangeScript.Instance.FadeOut("Title");
        }
	}
}
