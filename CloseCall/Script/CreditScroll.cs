﻿using UnityEngine;
using System.Collections;

public class CreditScroll : MonoBehaviour
{
    public float scrollStartPoint_Y;
    public float scrollSpeed_Y;
    private float position_Y;

	// Use this for initialization
	void Start ()
    {
        transform.localPosition = new Vector3(0, scrollStartPoint_Y, 0);
        position_Y = scrollStartPoint_Y;   
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.localPosition.y <= 3000)
        {
            transform.localPosition = new Vector3(0, position_Y, 0);
            position_Y += scrollSpeed_Y;
        }
	}
}
