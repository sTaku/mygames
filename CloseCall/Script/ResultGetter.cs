﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ResultGetter : MonoBehaviour
{
    public bool setResult;
    string nowScene;
    static string playedScene;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        nowScene = SceneManager.GetActiveScene().name;
        if (nowScene.Contains("Stage"))
        {
            playedScene = nowScene;
        }
        //Debug.Log(nowScene);
        //Debug.Log(playedScene);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public string NowScene()
    {
        return nowScene;
    }

    public string PlayedScene()
    {
        return playedScene;
    }
}
