﻿using UnityEngine;
using System.Collections;

public class CountDownScript : MonoBehaviour {

	private RectTransform m_RectTransform;
    [SerializeField]
    float waitTime;
    void Awake()
    {
        m_RectTransform = GetComponent<RectTransform>();
    }

    // Use this for initialization
    void Start()
    {
        LeanTween.scale(m_RectTransform, new Vector3(1f, 1f, 1f), 0.8f)
            .setDelay(waitTime)
            .setEase(LeanTweenType.easeInOutExpo)
            .setOnComplete(() => {
                LeanTween.scale(m_RectTransform, new Vector3(2, 0, 1f), 0.1f)
            .setDelay(0.2f);
                if (m_RectTransform.tag != "Start") SoundManger.Instance.PlaySE(5);
                else SoundManger.Instance.PlaySE(6);
            });
        if (m_RectTransform.tag == "Start") return;
        LeanTween.rotate(m_RectTransform, 360 * 3f, 0.8f)
            .setDelay(waitTime);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
