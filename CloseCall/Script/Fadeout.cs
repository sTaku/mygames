﻿using UnityEngine;
using System.Collections;

public class Fadeout : MonoBehaviour
{
    [SerializeField]
    Vector2 position;
    [SerializeField]
    float waitTime;


    float DestroyTime;
    private RectTransform fade;



    // Use this for initialization
    void Start () {
        fade = GetComponent<RectTransform>();
        LeanTween.move(fade, position, 0.3f)
        .setDelay(waitTime);
        
    }

    // Update is called once per frame
    void Update () {
             DestroyTime += Time.deltaTime;
            if (DestroyTime >= 2.3f)
            {
                Destroy(gameObject);
            }
    }
}
