﻿using UnityEngine;
using System.Collections;

public class TutorialManeger : MonoBehaviour
{

    
    public GameObject Arrow,Keyboard,hand;
    public GameObject[] Textobj;
    public Transform TextArea;
    public int EnemyCount = 0;
    public int TutorialCount = 0;
    public int TutorialEndCountMax;
    public int Countplus;
    public Transform[] EnemyArea;
    private GameObject sceneChange;
    public int TextCount;

    // Use this for initialization
    void Start()
    {
        Arrow.SetActive(false);
        Keyboard.SetActive(false);
        hand.SetActive(false);
        sceneChange = GameObject.Find("SceneManager");
    }

    // Update is called once per frame
    void Update()
    {
        if (GamePlayManagerScript.GamePlayManager.waitFrag == false) return;
        TutorialManege();
    }
    IEnumerator Enemy()
    {
        EnemyCount++;
        yield return new WaitForSeconds(2);
        EnemyArea[0].GetComponent<EnemyArea>().EnemyGeneration();
    }
    IEnumerator Enemy2()
    {
        EnemyCount++;
        TextCount++;
        yield return new WaitForSeconds(3);
        EnemyArea[1].GetComponent<EnemyArea>().EnemyGeneration();
    }
    public void TutorialCountplus()
    {
        TutorialCount = TutorialCount + Countplus;
    }

    public void TutorialManege()
    {
        if ((TutorialCount == 0) && (TextCount == 0))
        {
            Arrow.SetActive(true);
            Instantiate(Textobj[0], TextArea.position, Quaternion.Euler(0, 0, 0));
            SoundManger.Instance.PlaySE(0);
            TextCount++;
        }
        if ((TutorialCount == 1) && (TextCount == 0))
        {
            TextCount++;
            Arrow.SetActive(false);
            Instantiate(Textobj[1], TextArea.position, Quaternion.Euler(0, 0, 0));
            SoundManger.Instance.PlaySE(0);
        }
        if ((TutorialCount == 2) && (TextCount == 0))
        {
            TextCount++;
            Instantiate(Textobj[2], TextArea.position, Quaternion.Euler(0, 0, 0));
            SoundManger.Instance.PlaySE(0);
            Keyboard.SetActive(true);
            hand.SetActive(true);
        }
        if ((TutorialCount == 3) && (TextCount == 0))
        {
            TextCount++;
            Instantiate(Textobj[3], TextArea.position, Quaternion.Euler(0, 0, 0));
            SoundManger.Instance.PlaySE(0);
        }
        if ((TutorialCount == 4) && (EnemyCount == 0) && (TextCount == 0))
        {
            StartCoroutine("Enemy");
        }
        if ((TutorialCount == 5) && (TextCount == 0))
        {
            TextCount++;
            Instantiate(Textobj[4], TextArea.position, Quaternion.Euler(0, 0, 0));
            SoundManger.Instance.PlaySE(0);
        }
        if ((TutorialCount == 6) && (TextCount == 0))
        {
            TextCount++;
            Instantiate(Textobj[5], TextArea.position, Quaternion.Euler(0, 0, 0));
            SoundManger.Instance.PlaySE(0);
        }
        if ((TutorialCount == 7) && (TextCount == 0))
        {
            if (EnemyCount < 3)
            {
                StartCoroutine("Enemy2");
            }
        }

        if (TutorialCount >= TutorialEndCountMax)
        {
            SceneChangeScript.Instance.FadeOut("Result");
        }

        if (TutorialCount == 10)
        {
            Arrow.SetActive(false);
            if (EnemyCount < 2)
            {
                StartCoroutine("Enemy");
                StartCoroutine("Enemy2");
            }
        }
    }
}