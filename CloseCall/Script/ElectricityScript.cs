﻿using UnityEngine;
using System.Collections;

public class ElectricityScript : MonoBehaviour
{
    private GameObject parent;
    public Vector2 toPoint;
    public float moveTime;
    public GameObject BurstParticle;

    // Use this for initialization
    void Start ()
    {
        //親オブジェクトを取得
        parent = gameObject.transform.parent.gameObject;
        toPoint = parent.GetComponent<OutputPorts>().toPoint.position;
        moveTime = parent.GetComponent<OutputPorts>().moveTime;
        Move(toPoint, moveTime);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!GetComponent<Renderer>().isVisible)
        {
            Destroy(this.gameObject);
        }
    }

    public void Move(Vector2 toMove, float time)
    {
        LeanTween.move(gameObject, toPoint, moveTime)
            //.setLoopType(LeanTweenType.)
            .setOnComplete(() =>
            {
                Move(toMove, time);
            });
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Switch")
        {
            toPoint = other.GetComponent<ElectricityRote>().MovePoint();
            moveTime = other.GetComponent<ElectricityRote>().MoveTime();
        }

        if (other.tag == "Princess" )
        {
            Instantiate(BurstParticle, this.transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(gameObject);
            SoundManger.Instance.PlaySE(4);

        }
        if (other.tag == "Enemy")
        {
            Destroy(gameObject);

        }
    }
}
