﻿using UnityEngine;
using System.Collections;

public class FlashScript : MonoBehaviour {

    private float m_time;
    public GameObject keyboardObj;
    

    //Use this for initialization
	void Start () {
        keyboardObj.SetActive(true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_time += Time.deltaTime;
        if (m_time > 1)
        {
            StartCoroutine("Flash");
            m_time = 0;
        }
    }

    IEnumerator Flash()
    {
        keyboardObj.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        keyboardObj.SetActive(false);
    }
}
