﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GamePlayManagerScript : MonoBehaviour
{
    protected static GamePlayManagerScript playManager;
    public GameObject BGM;

    public static GamePlayManagerScript GamePlayManager
    {
        get
        {
            if (playManager == null)
            {
                playManager = (GamePlayManagerScript)FindObjectOfType(typeof(GamePlayManagerScript));
                if (playManager == null)
                {
                    Debug.LogError("SoundManager Instance Error");
                }
            }

            return playManager;
        }
    }

    //ゲームが始まるまでの待機時間
    public float waitTime;
    //このフラグがtrueになればゲームが開始する
    public bool waitFrag;
	// Use this for initialization
	void Start ()
    {
        waitFrag = false;
        StartCoroutine("WaitStart");

    }
	
	// Update is called once per frame
	void Update ()
    {
	}

    //指定した時間になるまでゲームを開始させない
    IEnumerator WaitStart()
    {
        yield return new WaitForSeconds(waitTime);
        waitFrag = true;
        BGM.SetActive(true);
    }
}
