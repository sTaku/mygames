﻿using UnityEngine;
using System.Collections;

public class ClearText : MonoBehaviour {

    private RectTransform m_RectTransform;
    void Awake()
    {
        m_RectTransform = GetComponent<RectTransform>();
    }

    // Use this for initialization
    void Start() {
        Loop();
    }
	// Update is called once per frame
	void Update () {
	
	}
    void Loop()
    {
        LeanTween.scale(m_RectTransform, new Vector3(1f, 1f, 1f), 0.8f)
                   .setDelay(1)
                   .setEase(LeanTweenType.easeInOutExpo)
                   .setOnComplete(() => {
                       LeanTween.scale(m_RectTransform, new Vector3(0.9f, 0.9f, 0.9f), 1.5f).setDelay(0.5f)
                       .setLoopType(LeanTweenType.pingPong);
                   });
    }
}
