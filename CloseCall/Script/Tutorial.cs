﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial : MonoBehaviour {


    public GameObject Image1, Image2, Image3,Image4,Image5;
    public TutorialManeger m_TutorialMane;
    private GameObject TutorialManeobj;

	// Use this for initialization
	void Start () {
        TutorialManeobj = GameObject.Find("TutorialMane");
        m_TutorialMane = TutorialManeobj.GetComponent<TutorialManeger>();


        Image1.GetComponent<TutorialDescriptioScript>().enabled = false;
        Image2.GetComponent<TutorialDescriptioScript>().enabled = false;
        Image3.GetComponent<TutorialDescriptioScript>().enabled = false;
        Image4.GetComponent<TutorialDescriptioScript>().enabled = false;
        Image5.GetComponent<TutorialDescriptioScript>().enabled = false;
        Image1.GetComponent<FlashScript>().enabled = false;

    }

    // Update is called once per frame
    void Update() {
        if (GamePlayManagerScript.GamePlayManager.waitFrag == false) return;
        if (m_TutorialMane.TutorialCount == 0)
        {
            Image1.SetActive(true);
            Image1.GetComponent<TutorialDescriptioScript>().enabled = true;
            Image1.GetComponent<FlashScript>().enabled = true;
        }

        if (m_TutorialMane.TutorialCount == 1)
        {
            Image1.GetComponent<TutorialDescriptioScript>().Close();
            //Image1.SetActive(false);
            Image2.SetActive(true);
            Image2.GetComponent<TutorialDescriptioScript>().enabled = true;
        }
        if(m_TutorialMane.TutorialCount == 2)
        {
            // Image2.SetActive(false);
            Image2.GetComponent<TutorialDescriptioScript>().Close();
            Image3.SetActive(true);
            Image3.GetComponent<TutorialDescriptioScript>().enabled = true;
        }
        if(m_TutorialMane.TutorialCount == 3)
        {
            //Image3.SetActive(false);
            Image3.GetComponent<TutorialDescriptioScript>().Close();
            Image4.SetActive(true);
            Image4.GetComponent<TutorialDescriptioScript>().enabled = true;
        }        
        if(m_TutorialMane.TutorialCount == 5)
        {
            //Image4.SetActive(false);
            Image4.GetComponent<TutorialDescriptioScript>().Close();
            Image5.SetActive(true);
            Image5.GetComponent<TutorialDescriptioScript>().enabled = true;
        }
        if(m_TutorialMane.TutorialCount == 9)
        {
            Image2.SetActive(true);
            Image2.GetComponent<TutorialDescriptioScript>().enabled = true;
        }
        
    }

    public void OnMouseDown()
    {
        TutorialManeobj.GetComponent<TutorialManeger>().TutorialCountplus();
    }

    
}
