﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class CameraEffect : MonoBehaviour {

    private NoiseAndScratches m_Noice;
    public bool deepNoice;

	// Use this for initialization
	void Start () {
        m_Noice = Camera.main.GetComponent<NoiseAndScratches>();
        m_Noice.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
        //noiseの濃度変更
        if(deepNoice == true)
        {
            m_Noice.grainIntensityMax++;
        }
        if(deepNoice == false)
        {
            m_Noice.grainIntensityMax -= Time.deltaTime;
        }
        Effect();
	}

    //noiseの濃度切り替え
    public void Effect()
    {
        if(m_Noice.grainIntensityMax > 5)
        {
            deepNoice = false;
        }

        if(m_Noice.grainIntensityMax <= 1)
        {
            deepNoice = true;
        }
    }
}
