﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyCounter : MonoBehaviour {

    public int EnemyCount;
    private int EnemyCountMax;
    private Text m_test;
    private GameObject sceneChange;
	// Use this for initialization
	void Start () {
        EnemyCountMax = EnemyCount;
        sceneChange = GameObject.Find("SceneManager");
        m_test = GetComponent<Text>();
        m_test.text = ("×"+(int)EnemyCount).ToString();
        m_test.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {
        if(EnemyCount <= EnemyCountMax/ 2)
        {
            m_test.color = Color.yellow;
        }

        if (EnemyCount <= EnemyCountMax / 3)
        {
            m_test.color = Color.green;
        }

        if (EnemyCount <= 0)
        {
            m_test.color = Color.white;
            sceneChange.GetComponent<SceneChangeScript>().FadeOut("Result");
        }
	}
    public void minusCount()
    {
        EnemyCount--;
        GetComponent<Text>().text = ("×" + (int)EnemyCount).ToString();
    }
}
