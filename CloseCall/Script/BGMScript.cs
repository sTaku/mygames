﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BGMScript : MonoBehaviour
{
    public int bgmNumber;
    private string nowScene;
    private GameObject core;

    void Awake()
    {
        /*----ここの処理をAwake()から動かさない----*/
        nowScene = SceneManager.GetActiveScene().name;
        core = GameObject.Find("Princess");
        if (nowScene.Contains("Result"))
        {
            if (core == null)
            {
                bgmNumber = 3;
            }
            else
            {
                bgmNumber = 2;
            }
        }
        /*-----------------------------------------*/

        GameObject[] obj = GameObject.FindGameObjectsWithTag("BGM");
        if (obj.Length > 1)
        {
            //既に存在しているなら削除
            Destroy(gameObject);
        }
        else
        {
            //音管理はシーン遷移では破棄させない
            //DontDestroyOnLoad(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        SoundManger.Instance.PlayBGM(bgmNumber);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
