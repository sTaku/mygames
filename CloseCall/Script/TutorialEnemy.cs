﻿using UnityEngine;
using System.Collections;

public class TutorialEnemy : MonoBehaviour{

    private GameObject parent;
    private GameObject EnemyCounter;
    public GameObject TutorialMane;
    public Vector2 EnemytoPoint, EnemytoPoint2;
    public float EnemymoveTime, EnemymoveTime2;
    public GameObject BurstParticle;

    public Vector2 temp;

    // Use this for initialization
    void Start()
    {
        parent = gameObject.transform.parent.gameObject;
        EnemytoPoint = parent.GetComponent<EnemyArea>().EnemytoPoint.position;
        EnemymoveTime = parent.GetComponent<EnemyArea>().EnemymoveTime;
        EnemytoPoint2 = parent.GetComponent<EnemyArea>().EnemytoPoint2.position;
        EnemymoveTime2 = parent.GetComponent<EnemyArea>().EnemymoveTime2;
        EnemyCounter = GameObject.Find("EnemyCounter");
        TutorialMane = GameObject.Find("TutorialMane");
        float random = Random.Range(0, 2);
        if (random == 0)
        {
            Move(EnemytoPoint, EnemymoveTime);
        }
        if (random == 1)
        {
            Move(EnemytoPoint2, EnemymoveTime2);
        }

        temp = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<Renderer>().isVisible)
        {
            Destroy(this.gameObject);
            TutorialMane.GetComponent<TutorialManeger>().TutorialCount++;
            EnemyCounter.GetComponent<EnemyCounter>().minusCount();
        }
    }

    public void Move(Vector2 toMove, float time)
    {

        LeanTween.move(gameObject, EnemytoPoint, EnemymoveTime)
       //.setLoopType(LeanTweenType.)
       .setOnComplete(() =>
       {
           Move(toMove, time);
       });

        if (temp.x != transform.position.x)
        {
            int a = 1;
        }
        temp = transform.position;
    }

    public void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "Switch")
        {
            EnemytoPoint = col.GetComponent<EnemyRote>().MovePoint();
            EnemymoveTime = col.GetComponent<EnemyRote>().MoveTime();
        }


        if ((col.tag == "Electricity") || (col.tag == "Princess"))
        {
            Instantiate(BurstParticle, this.transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(this.gameObject);
            TutorialMane.GetComponent<TutorialManeger>().TutorialCount++;
            EnemyCounter.GetComponent<EnemyCounter>().minusCount();
            SoundManger.Instance.PlaySE(7);
        }
    }

}

