﻿using UnityEngine;
using System.Collections;
using System;

public class SoundManger : MonoBehaviour {

    protected static SoundManger instance;

    public static SoundManger Instance
    {
        get
        {
            if(instance == null)
            {
                instance = (SoundManger)FindObjectOfType(typeof(SoundManger));
                if(instance == null)
                {
                    Debug.LogError("SoundManager Instance Error");
                }
            }

            return instance;
        }
    }

    //音量
    public SoundVolume volume = new SoundVolume();


    //AudioSource

    //BGM
    private AudioSource BGMsource;
    //SE
    private AudioSource[] SEsources = new AudioSource[16];
    //Voice
    private AudioSource[] VoiceSources = new AudioSource[16];

    //AudioClip

    //BGM
    public AudioClip[] BGM;
    //SE
    public AudioClip[] SE;
    //Voice
    public AudioClip[] Voice;

    //BGMのFadeOutFlag
    public bool BGMFadeFlag;


    void Awake()
    {
        //全てのオーディオコンポーネントを追加する

        //BGM AudioSource
        BGMsource = gameObject.AddComponent<AudioSource>();
        //BGMはループを有効にする
        BGMsource.loop = true;

        //SEsource
        for(int i = 0; i < SEsources.Length; i++)
        {
            SEsources[i] = gameObject.AddComponent<AudioSource>();
        }

        for (int i = 0; i < VoiceSources.Length; i++)
        {
            VoiceSources[i] = gameObject.AddComponent<AudioSource>();
        }

        //音声source
        for(int i = 0; i < VoiceSources.Length; i++)
        {
            VoiceSources[i] = gameObject.AddComponent<AudioSource>();
        }

        //Fadeを実行しないようにfalseにする
        BGMFadeFlag = false;
    }

    void Update()
    {
        //FadeがtrueになったらFadeOutを実行させる
        if(BGMFadeFlag == true &&
            volume.BGM > 0)
        {
            volume.BGM -= 0.1f;
        }
        else if(BGMFadeFlag == false && volume.BGM <= 1.0f)
        {
            volume.BGM += 0.1f;
        }
        //音が聞こえなくなったらBGMを止める
        if(volume.BGM <= 0)
        {
            StopBGM();
            BGMFadeFlag = false;
        }

        //ミュート設定
        BGMsource.mute = volume.Mute;
        foreach(AudioSource source in SEsources)
        {
            source.mute = volume.Mute;
        }
        foreach(AudioSource source in VoiceSources)
        {
            source.mute = volume.Mute;
        }

        //ボリューム設定
        BGMsource.volume = volume.BGM;
        foreach(AudioSource source in SEsources)
        {
            source.volume = volume.SE;
        }
        foreach(AudioSource source in VoiceSources)
        {
            source.volume = volume.Voice;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneChangeScript.Instance.FadeOut("Title");
        }
    }


    //BGM再生

    //BGM再生
    public void PlayBGM(int index)
    {
        if (0 > index || BGM.Length <= index)
        {
            return;
        }

        //同じBGMの場合何もしない
        if (BGMsource == BGM[index])
        {
            return;
        }
        BGMsource.Stop();
        BGMsource.clip = BGM[index];
        BGMsource.Play();
        BGMFadeFlag = false;
    }

    public void StopBGM()
    {

        BGMsource.Stop();
        BGMsource.clip = null;
        
    }

    public void FadeOutBGM()
    {
        BGMFadeFlag = true;
    }
    //SE再生
    
    //SE再生
    public void PlaySE(int index) 
    {
        if(0 > index || SE.Length <= index)
        {
            return;
        }

        //再生中で無いAudioSourceで鳴らす
        foreach(AudioSource source in SEsources)
        {
            if(false == source.isPlaying)
            {
                source.clip = SE[index];
                source.Play();
                return;
            }
        }
    }

    //SE停止
    public void StopSE()
    {
        //全てのSE用のAudioSourceを停止する
        foreach(AudioSource source in SEsources)
        {
            source.Stop();
            source.clip = null;
        }
    }


    //音声再生

    //音声再生
    public void PlayVoice(int index)
    {
        if( 0 > index || Voice.Length <= index )
        {
            return;
        }

        //再生中で無いAudioSourceで鳴らす
        foreach(AudioSource source in SEsources)
        {
            if( false == source.isPlaying)
            {
                source.clip = Voice[index];
                source.Play();
                return;
            }
        }
    }

    //音声停止
    public void StopVoice()
    {
        //全ての音声用のAudioSourceを停止する
        foreach(AudioSource source in VoiceSources)
        {
            source.Stop();
            source.clip = null;
        }
    }
}

//音量クラス
[Serializable]
public class SoundVolume
{
    public float BGM = 1.0f;
    public float Voice = 1.0f;
    public float SE = 1.0f;
    public bool Mute = false;

    public void Init()
    {
        BGM = 1.0f;
        Voice = 1.0f;
        SE = 1.0f;
        Mute = false;
    }
}
