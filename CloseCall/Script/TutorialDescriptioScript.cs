﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialDescriptioScript : MonoBehaviour
{
    [SerializeField]
    float num;
    [SerializeField]
    float waitTime;
    [SerializeField]
    float waitTime2;
    public float m_time;
    public GameObject delte;
    private GameObject TutorialManeobj;
    // Use this for initialization
    void Start ()
    {
        //delte = GameObject.Find("Delte");
        TutorialManeobj = GameObject.Find("TutorialMane");
        gameObject.GetComponent<Transform>().localScale = new Vector3(0, 0, 1);
        Open();
	}
	
	// Update is called once per frame
	void Update () {
         m_time += Time.deltaTime;

        if (m_time > 1)
        {
            m_time = 0;
            StartCoroutine("Flash");
        }
    }

    void Open()
    {
        LeanTween.scale(gameObject, new Vector3(0.03f, 2, 1), num)
            .setDelay(waitTime)
            .setOnComplete(() =>
            {
                LeanTween.scale(gameObject, new Vector3(0.05f, 1, 1), num)
                .setOnComplete(() =>
                {
                    LeanTween.scale(gameObject, new Vector3(1, 1, 1), num);
                });
            });
    }

   public void Close()
    {
        LeanTween.scale(gameObject, new Vector3(0.03f, 2, 1), num).setDelay(waitTime2).setOnComplete(() =>
         {
             LeanTween.scale(gameObject, new Vector3(0.05f, 1, 1), num).setOnComplete(() =>
             {
                 TutorialManeobj.GetComponent<TutorialManeger>().TextCount--;
                 Destroy(gameObject);
             });
         });
    }
    IEnumerator Flash()
    {
        delte.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        delte.SetActive(false);
    }

    public void OnMouseDown()
    {
        TutorialManeobj.GetComponent<TutorialManeger>().TutorialCountplus();
        Close();
    }
}
