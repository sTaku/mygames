﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RturnTime : MonoBehaviour {
    public  float timeout;
    [SerializeField]
    private float titletime;
    private GameObject princess;


    void Awake()
    {
        if(SceneManager.GetActiveScene().name == "Credit")
        {
            GameObject returnTime = GameObject.Find("ReturnTime");
            if(returnTime != null) Destroy(returnTime);
            return;
        }
        GameObject[] obj = GameObject.FindGameObjectsWithTag("Return");
        if (obj.Length > 1)
        {
            princess = GameObject.Find("Princess");
            if (princess != null)
            {
                Destroy(princess);
            }
            //既に存在しているなら削除
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
    void Start()
    {
    }

    // Use this for initialization
    public void Update()
    {
        titletime += Time.deltaTime;
        if (titletime >= timeout)
        {
            if (SceneManager.GetActiveScene().name=="Title")
            {
                SceneChange("PV");
            }

            else if (SceneManager.GetActiveScene().name == "PV")
            {
                SceneChange("Title");
            }

            else
            {
                Destroy(princess);
                SceneChange("Title");
            }
         }

        

        if(Input.anyKey)
        {
            titletime = 0.0f;
        }
        //ユーザーのキー操作判定
        if (Input.GetMouseButtonDown(0)|| Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
        {
            titletime = 0.0f;
        }
    }

    void SceneChange(string name)
    {
        SceneChangeScript.Instance.FadeOut(name);
        titletime = 0.0f;
    }
}
