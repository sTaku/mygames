﻿using UnityEngine;
using System.Collections;

public class OutputPorts : MonoBehaviour
{

    [SerializeField]
    private GameObject electricity;
    public Transform toPoint;
    public float moveTime;
    public GameObject OffObj;
    public GameObject OnObj;
    public int PortNumber;
    public int MaxElec;

    private GameObject prefab;
    private GameObject obj;

    [SerializeField]
    private GameObject anime;
    
    // Use this for initialization
    void Start()
    {
        anime=(GameObject)Resources.Load("coaBlock_0");

    }

    // Update is called once per frame
    void Update()
    {
        if (GamePlayManagerScript.GamePlayManager.waitFrag == false)
        {
            OnObj.SetActive(true);
            OffObj.SetActive(false);
            return;
        } 
        int Count = this.gameObject.transform.childCount;

        if (Count >= (2 + MaxElec))
        {
            return;
        }
        else
        {
            if (PortNumber == 1)
            {
                if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 2)
            {
                if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 3)
            {
                if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 4)
            {
                if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 5)
            {
                if (Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 6)
            {
                if (Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Keypad6))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 7)
            {
                if (Input.GetKeyDown(KeyCode.Alpha7) || Input.GetKeyDown(KeyCode.Keypad7))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 8)
            {
                if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Keypad8))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
            else if (PortNumber == 9)
            {
                if (Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9))
                {
                    OnObject();
                }
                else
                {
                    OffObject();
                }
            }
        }
    }

    private void OnObject()
    {
        OnObj.SetActive(true);
        OffObj.SetActive(false);
        Production();
        Instantiate(anime, this.transform.position, Quaternion.Euler(0,0,0));
    }

    private void OffObject()
    {
        OnObj.SetActive(false);
        OffObj.SetActive(true);
    }
    
    public void Production()
    {
        obj = (GameObject)Instantiate(electricity, this.transform.position, Quaternion.Euler(0, 0, 0));
        obj.transform.parent = transform;
        SoundManger.Instance.PlaySE(2);
    }
}
