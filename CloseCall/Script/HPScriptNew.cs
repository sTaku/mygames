﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class HPScriptNew : MonoBehaviour
{
    private int hp2;
    private GameObject obj;

    public int wait_FadeOut;
    public GameObject hp_2;
    public GameObject princess;
    public GameObject sceneChange;
    public GameObject frame;

    private NoiseAndScratches NoiseFade;
    // Use this for initialization
    void Start ()
    {
        NoiseFade = Camera.main.GetComponent<NoiseAndScratches>();
        NoiseFade.enabled = false;
        DrawNewGauge();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (hp2 <= 0)
        {
            wait_FadeOut--;
        }
        if (wait_FadeOut < 0)
        {
            NoiseFade.enabled = true;
            NoiseFade.grainIntensityMax++;
            sceneChange.GetComponent<SceneChangeScript>().FadeOut("Result");
        }

        if (princess != null)
        {
            hp2 = princess.GetComponent<Princess>().Hp2();
            if (hp2 <= 5)
            {
                frame.GetComponent<Animator>().enabled = true;
            }
        }
    }

    void DrawNewGauge()
    {
        hp2 = princess.GetComponent<Princess>().Hp2();
        for (int i = 0; i < hp2; i++)
        {
            obj = (GameObject)Instantiate(
                hp_2,
                transform.position,
                Quaternion.identity);
            //obj.transform.parent = transform;
            obj.transform.SetParent(transform);
            obj.transform.localPosition = transform.position + new Vector3(0.0f, i * 18.0f, 0.0f); ;
            obj.transform.localScale = new Vector3(0.8f, 0.5f, 1.0f);
            obj.GetComponent<HPChild>().hpCounter = i + 1;
        }
    }
}
