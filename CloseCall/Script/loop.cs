﻿using UnityEngine;
using System.Collections;

public class loop : MonoBehaviour {
    Vector2 start_posi;
    Vector2 move_posi;
    public bool xloop;
    public bool Arrowloopx;
    public bool Arrowloopy;

    // Use this for initialization
    void Start()
    {
        start_posi = transform.position; //スタート地点を記憶
    }

    // Update is called once per frame
    void Update()
    {
        loopmove();
        Arrowloop();
    }

    void loopmove()
    {
        if (xloop == true)
        {
            move_posi = new Vector2(0, Mathf.Sin(Time.time) / 2);
            this.transform.localPosition = start_posi + move_posi / 2;

        }
    }
    
    void Arrowloop()
    {
        if (Arrowloopx == true)
        {
            move_posi = new Vector2(0, Mathf.Sin(Time.time));
            this.transform.localPosition = start_posi + move_posi;

        }
        if (Arrowloopy == true)
        {
            move_posi = new Vector2(Mathf.Sin(Time.time), 0);
            this.transform.localPosition = start_posi + move_posi;
        }
    }
}
